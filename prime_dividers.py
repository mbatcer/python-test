from functools import reduce

def greatest_divider(x, accum):
    for i in range(x//2, 0, -1):
        if x%i == 0:
            accum.append(x//i)
            return i

def prime_dividers(n):
    accum = []
    while True:
        n = greatest_divider(n, accum)
        if n == 1:
            break
    return accum

def main():
    try:
        n = int(input("Print an integer greater than 1 (Ctrl+C to exit): "))
        if n < 2:
            raise ValueError()
    except ValueError:
        print("Wrong input. ")
        return
    dividers = prime_dividers(n)
    product = reduce(lambda x, y: x*y, dividers)
    print("Prime factorization: ", \
          product, '=', ' * '.join(str(j) for j in dividers), \
          end = '\n======================================================\n')

print("""
======================================================
             Prime factorization program
======================================================
""")
while True:
    main()
