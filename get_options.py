'''
Задание:
Напишите простую реализацию функции для разбора параметров
командной строки (getopt).
Полагаем, что нам требуется структура данных, содержащая
заранее известный допустимый набор параметров различного
типа - строки, целые, числа с плавающей точкой и т. д.,
а также признак, является ли этот параметр обязательным.
Полагаем, что параметры могут передаваться только в "длинном"
формате с обязательным "--" перед именем параметра (то есть
"--my-option-name my-option-val", "--my-option-name=my-option-val",
"--my-boolean-option").
Параметров может быть несколько.
Функция должна обязательно уметь обрабатывать параметр "--help"
(если он указан в списке параметров при вызове функции),
выводящий пример использования (необязательные параметры должны
выводиться в квадратных скобках).
'''

import re

def parse_params(params_string):
    par_dict = {}
    pattern = '--([\w\d_-]+)(\s+[\w\d\s\._]+)?'
    matches = re.findall(pattern, params_string)
    for m in matches:
        name = m[0]
        value = m[1].strip()
        if value == '':
            value = None
        par_dict[name] = value
    return par_dict

def validate_params(params, accepted_params):
    required_params = [name for name, data in accepted_params.items() if data['required']]
    for par_name, par_value in params.items():
        if par_name == 'help':
            show_help(accepted_params)
            return
        if not par_name in accepted_params.keys():
            msg_template = "Error: unknown parameter \"--{0}\""
            raise ValueError(msg_template.format(par_name))
        par_type = accepted_params[par_name]['type']
        if par_type != 'bool' and not par_value:
            msg_template = "Error: value not passed for parameter \"--{0}\""
            raise ValueError(msg_template.format(par_name))
        if par_type == 'int' and not par_value.lstrip('-').isdigit():
            msg_template = "Error: wrong value passed for parameter" + \
                                " \"--{0}\", must be an integer"
            raise ValueError(msg_template.format(par_name))
    for par_name in required_params:
        if not par_name in params.keys():
            msg_template = "Error: required parameter \"--{0}\" is missing"
            raise ValueError(msg_template.format(par_name))

def show_help(accepted_params):
    print("Usage: ", end='')
    for par_name, par_data in accepted_params.items():
        template = '--{0}'
        if par_data['type'] != 'bool':
            template += ' <{1}>'
        if not par_data['required']:
            template = '[ ' + template + ' ]'
        print(template.format(par_name, par_data['type']), end=' ')

def get_params(params_string):
    accepted_params = {
        'option-1': { 'type': 'string', 'required': True,
                             'description': "Set option-1" },
        'option-2': { 'type': 'int', 'required': True,
                          'description': "Set option-2" },
        'option-3': { 'type': 'bool', 'required': False,
                              'description': "Enable option-3" },
        'option-4': { 'type': 'string', 'required': False,
                          'description': "Set option-4" }
    }
    params = parse_params(params_string)
    try:
        validate_params(params, accepted_params)
        print("Params passed: ", params)
        return params
    except ValueError as e:
        print(str(e))


params_string = input("Type in params string: ")
get_params(params_string)
# e.g. for input "--option-1 abcd --option-2 4545 --option-3"
# prints "{'option-2': '4545', 'option-1': 'abcd', 'option-3': None}"


