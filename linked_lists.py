'''
Задание:
Имеется простой односвязный список размера N.
Необходимо реализовать алгоритм, который перепаковывает список так,
чтобы за первым элеметом следовал последний, затем второй,
затем предпоследний и т. д.
Пример работы алгоритма:
исходный список: 1-2-3-4-5-6-7-8
перепакованный список: 1-8-2-7-3-6-4-5.
'''
class Node:
    def __init__(self, value = None, next = None):
        self.value = value
        self.next = next

    def insertAfter(self, x):
        x.next = self
        return self

class LinkedList:
    def __init__(self, first = None):
        self.first = first

    def __str__(self):
        if self.first != None:
            current = self.first
            out = 'LinkedList[' + str(current.value)
            while current.next != None:
                current = current.next
                out += ', ' + str(current.value)
            return out + ']'
        return 'LinkedList[]'


# создание простого односвязного списка из N элементов
N = 9
L = LinkedList(Node(1))
last = L.first
for i in range(2, N+1):
    new = Node(i)
    new.insertAfter(last)
    last = new
print('Исходный список:\n\t', L)

# перепаковка списка
offset = N
next_from_start = L.first
while offset > 1:
    tmp = next_from_start
    for j in range(1, offset):
        tmp = tmp.next
    next_from_end = tmp
    next_from_end.next = next_from_start.next
    next_from_start.next = next_from_end
    next_from_start = next_from_end.next
    offset -= 2
next_from_start.next = None
print('Перепакованный список:\n\t', L)
