'''
Задание:
Напишите код, который решает следующую задачу:
Имеется вектор размера N. Необходимо разделить его
на M < N равных частей (то есть чтобы количество элементов
в каждой части было одинаковым). Части не должны
пересекаться и должны покрывать весь вектор (исключение -
допускаются промежутки от начала вектора до начала первой части
и от конда последней части до конца вектора, но в этом случае
необходимо добиться, чтобы разница в величине этих промежутков
была минимальной).
Результатом должны являться индексы начала и конца каждой части
(либо вывод на экран, либо сохранение в любую структуру данных).
'''

def chunks_indexes(vector, n):
    m = len(vector)
    # n is chunk size
    if n > m:
        raise ValueError()
    offset = m%n // 2
    indexes = []
    for i in range(m//n):
        indexes.append((i*n + offset, (i+1)*n - 1 + offset))
    return indexes

# Example

vector = range(20)
# Print 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
print("Input vector:")
print(', '.join(str(i) for i in vector))

print("Indexes of chunks in input vector (starting at 0):")

print("  chunks with 5 elements each:", chunks_indexes(vector, 5))

print("  chunks with 7 elements each:", chunks_indexes(vector, 7))
