'''
Задание:
Напишите функцию, которая перебирает натуральные числа от 1 до N
включительно и раскладывает каждое число на множители. Результат
можно выводить на экран либо копить в любой структуре данных.
'''

from functools import reduce

def greatest_divider(x, accum):
    for i in range(x//2, 0, -1):
        if x%i == 0:
            accum.append(x//i)
            return i

def prime_dividers(n):
    if n == 1:
        return [1]
    accum = []
    while True:
        n = greatest_divider(n, accum)
        if n == 1:
            break
    return accum

def iterate(n):
    if n < 1:
        raise ValueError
    for i in range(1, n+1):
        dividers = prime_dividers(i)
        product = reduce(lambda x, y: x*y, dividers)
        print(product, '=', ' * '.join(str(j) for j in dividers))
    
    
    

print("""
======================================================
             Prime factorization program
======================================================
""")
n = int(input("Print an integer greater than 0: "))
try:
    iterate(n)  
except ValueError:
    print("Wrong input. ")
