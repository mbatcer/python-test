# -*- encoding: utf-8 -*-
import json
from pprint import pprint

filename = input(u"Введите имя файла: ")

with open(filename) as data_file:    
    data = json.load(data_file)

definitions = data['definitions']
tables = {}
exclude_props = {
    "created_by",
    "created_date",
    "created_date_tz",
    "deleted_by",
    "deleted_date",
    "deleted_date_tz",
    "is_deleted",
    "last_modified_date",
    "last_modified_user",
    "modified_date_tz"
}
row_template = "{0}\t{1}\t{2}"
    

for entity_name, entity in definitions.items():
    if not 'properties' in entity:
        continue
    tables[entity_name] = []
    for prop_name, prop in entity['properties'].items():
        if prop_name in exclude_props or not 'type' in prop:
            continue
        if prop['type'] == 'array':
            if not '$ref' in prop['items']:
                continue
            ref_type = prop['items']['$ref'].replace('#/definitions/', '')
            prop['type'] = u'массив из ' + ref_type
        new_row = row_template.format(prop_name, prop['type'], '')
        tables[entity_name].append(new_row)
    tables[entity_name].sort()
    table_header = row_template.format(u'Имя', u'Тип', u'Описание')
    tables[entity_name].insert(0, table_header)

with open('output.txt', 'w', encoding='utf-8') as output_file:
    for heading, strings in tables.items():
        print('', file=output_file)
        print(heading, file=output_file)
        for s in strings:
            print(s, file=output_file)


